package com.luke.TestApi.controller;

import com.luke.TestApi.exception.ArtistNotFoundException;
import com.luke.TestApi.exception.PageNotFoundException;
import com.luke.TestApi.model.Album;
import com.luke.TestApi.model.Artist;
import com.luke.TestApi.service.ArtistService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ArtistControllerTest {

    private List<Artist> artists = new ArrayList<>();

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private ArtistService artistService;

    @Autowired
    @InjectMocks
    private ArtistController artistController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();

        Artist oasis = Artist.builder().name("Oasis").genre("Brit Pop").build();
        Artist kLamar = Artist.builder().name("Kendrick Lamar").genre("Rap").build();
        Artist mozart = Artist.builder().name("Mozart").genre("Classical").build();
        artists.addAll(Arrays.asList(oasis, kLamar, mozart));
    }

    @Test
    public void shouldGetCorrectListOfArtistsAndRespondWith200() throws Exception {
        Page<Artist> artistPage = new PageImpl<>(artists);
        when(artistService.findPaginatedArtists(2, 0)).thenReturn(artistPage);

        mvc.perform(MockMvcRequestBuilders.get("/artists")
                .param("limit", "2")
                .param("offset", "0"))

                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(3)));
    }

    @Test
    public void shouldFailToGetPageRespondWith400() throws Exception {
        when(artistService.findPaginatedArtists(2, 20)).thenThrow(PageNotFoundException.class);

        mvc.perform(MockMvcRequestBuilders.get("/artists?limit=2?offset=20"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldGetArtistByNameAndRespondWith200() throws Exception {
        when(artistService.findArtistByName("Oasis")).thenReturn(artists.get(0));

        mvc.perform(MockMvcRequestBuilders.get("/artists/Oasis"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Oasis"));
    }

    @Test
    public void shouldFailToGetArtistByNameAndRespondWith400() throws Exception {
        when(artistService.findArtistByName("Oasis")).thenThrow(ArtistNotFoundException.class);

        mvc.perform(MockMvcRequestBuilders.get("/artists/Oasis"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldGetAllAlbumsByArtistNameAndRespondWith200() throws Exception {
        Album definitely_maybe = Album.builder().title("Definitely Maybe").build();
        Album morning_glory = Album.builder().title("What's the story of morning glory").build();
        when(artistService.findAllAlbumsByArtist("Oasis"))
                .thenReturn(Arrays.asList(definitely_maybe, morning_glory));

        mvc.perform(MockMvcRequestBuilders.get("/artists/Oasis/albums"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", hasSize(2)));
    }
}
