package com.luke.TestApi.repository;

import com.luke.TestApi.model.Album;
import com.luke.TestApi.model.Artist;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface AlbumRepository extends PagingAndSortingRepository<Album, Long> {
    List<Album> findAll();

    List<Album> findAllByArtist(@NotNull Artist artist);

    Optional<Album> findAlbumByTitle(String title);
}
