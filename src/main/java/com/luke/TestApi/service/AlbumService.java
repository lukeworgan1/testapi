package com.luke.TestApi.service;

import com.luke.TestApi.dto.AlbumDto;
import com.luke.TestApi.exception.AlbumNotFoundException;
import com.luke.TestApi.model.Album;
import com.luke.TestApi.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AlbumService {

    private AlbumRepository albumRepository;

    private ArtistService artistService;

    @Autowired
    public AlbumService(AlbumRepository albumRepository, ArtistService artistService) {
        this.albumRepository = albumRepository;
        this.artistService = artistService;
    }

    public List<Album> findAll() {
        return albumRepository.findAll();
    }

    public void addAlbum(String artist, AlbumDto albumDto) {
        Album newAlbum = Album.builder()
                .artist(artistService.findArtistByName(artist))
                .price(Double.valueOf(albumDto.getPrice()))
                .title(albumDto.getTitle())
                .sales(0)
                .releaseDate(LocalDateTime.now())
                .build();
        albumRepository.save(newAlbum);
    }

    public void sell(String albumTitle) {
        Optional<Album> optional = albumRepository.findAlbumByTitle(albumTitle);
        Album album = optional.orElseThrow(() -> new AlbumNotFoundException(albumTitle + " is not an album"));
        album.setSales(album.getSales() + 1);
        artistService.updateMoneyGenerated(album.getPrice(), album.getArtist().getName());
        albumRepository.save(album);
    }

    public Album findAlbumByName(String album) {
        return albumRepository.findAlbumByTitle(album)
                .orElseThrow(() -> new AlbumNotFoundException(album + " is not an album"));
    }
}
