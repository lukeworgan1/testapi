package com.luke.TestApi.service;

import com.luke.TestApi.exception.ArtistNameExistsException;
import com.luke.TestApi.exception.ArtistNotFoundException;
import com.luke.TestApi.model.Album;
import com.luke.TestApi.model.Artist;
import com.luke.TestApi.repository.AlbumRepository;
import com.luke.TestApi.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ArtistService {

    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;

    @Autowired
    public ArtistService(ArtistRepository artistRepository, AlbumRepository albumRepository) {
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
    }

    public Page<Artist> findPaginatedArtists(Integer limit, Integer offset) {
        if (limit >= 0) {
            Pageable pageable = PageRequest.of(offset, limit);
            return artistRepository.findAll(pageable);
        } else {
            throw new IllegalArgumentException("Page limit can't be less than 1");
        }
    }

    public Artist findArtistByName(String username) {
        Optional<Artist> optional = artistRepository.findArtistByName(username);
        return optional.orElseThrow(() -> new ArtistNotFoundException(username + " is not an artist"));
    }

    public void save(Artist newArtist) {
        Optional<Artist> optional = artistRepository.findArtistByName(newArtist.getName());
        if (optional.isPresent()) {
            throw new ArtistNameExistsException("An artist with that name already exists - name must be unique");
        } else {
            artistRepository.save(newArtist);
        }
    }

    public List<Album> findAllAlbumsByArtist(String artistName) {
        Optional<Artist> optional = artistRepository.findArtistByName(artistName);
        return albumRepository.findAllByArtist(optional
                .orElseThrow(() -> new ArtistNotFoundException(artistName + " is not an artist")));
    }

    public void updateMoneyGenerated(Double money, String name) {
        Optional<Artist> optional = artistRepository.findArtistByName(name);
        Artist artist = optional
                .orElseThrow(() -> new ArtistNotFoundException(name + " is not an artist"));
        artist.setMoney_generated(artist.getMoney_generated() + money);
        artistRepository.save(artist);
    }
}
