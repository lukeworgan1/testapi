package com.luke.TestApi.controller;

import com.luke.TestApi.dto.AlbumDto;
import com.luke.TestApi.exception.AlbumNotFoundException;
import com.luke.TestApi.model.Album;
import com.luke.TestApi.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/albums")
public class AlbumController {

    private AlbumService service;

    @Autowired
    public AlbumController(AlbumService service) {
        this.service = service;
    }

    @PostMapping("/{artist}/new")
    ResponseEntity<String> newAlbum(@PathVariable String artist, @RequestBody AlbumDto albumDto) {
        try {
            service.addAlbum(artist, albumDto);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Successfully added new album to " + artist);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }

    @GetMapping("/{album}")
    Album getAlbum(@PathVariable String album) {
        try {
            return service.findAlbumByName(album);
        } catch (AlbumNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }

    @PostMapping("/{album}/sale")
    ResponseEntity<String> sell(@PathVariable String album, @RequestParam Long fanId) {
        service.sell(album);
        return ResponseEntity.status(HttpStatus.OK).body("Sale has gone through");
    }
}
