package com.luke.TestApi.controller;

import com.luke.TestApi.exception.ArtistNotFoundException;
import com.luke.TestApi.model.Album;
import com.luke.TestApi.model.Artist;
import com.luke.TestApi.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/artists")
public class ArtistController {

    private ArtistService service;

    @Autowired
    public ArtistController(ArtistService service) {
        this.service = service;
    }

    @GetMapping
    Page<Artist> getListOfArtists(@RequestParam Integer limit,
                                  @RequestParam Integer offset) {
        try {
            return service.findPaginatedArtists(limit, offset);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }

    @PostMapping
    ResponseEntity<String> newArtist(@RequestBody Artist newArtist) {
        try {
            service.save(newArtist);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Successfully created a new artist");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }

    @GetMapping("/{name}")
    Artist getArtistByName(@PathVariable String name) {
        try {
            return service.findArtistByName(name);
        } catch (ArtistNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }

    @GetMapping("/{artist}/albums")
    List<Album> getAlbumsByArtist(@PathVariable String artist) {
        try {
            return service.findAllAlbumsByArtist(artist);
        } catch (ArtistNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Request", e);
        }
    }
}
