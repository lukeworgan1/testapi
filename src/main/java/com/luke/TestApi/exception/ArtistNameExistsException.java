package com.luke.TestApi.exception;

public class ArtistNameExistsException extends RuntimeException {
    public ArtistNameExistsException(String message) {
        super(message);
    }
}
