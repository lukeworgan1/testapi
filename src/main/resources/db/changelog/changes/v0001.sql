create table artist (
    id bigserial not null,
    username varchar(30) not null,
    artist_name varchar(50),
    artist_genre varchar(50),
    albums_recorded integer,
    primary key (id)
)