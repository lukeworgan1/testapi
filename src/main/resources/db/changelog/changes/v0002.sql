create table album (
    id bigserial not null,
    artist_id bigint not null references artist (id),
    title varchar(100) not null,
    releaseDate timestamp without time zone not null,
    sales integer,
    price float,
    primary key (id)
);

ALTER TABLE artist RENAME artist_name TO name;
ALTER TABLE artist RENAME artist_genre TO genre;
ALTER TABLE artist DROP COLUMN username;