create table fan (
    id bigserial not null,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    age integer not null,
    primary key (id)
);

create table artist_fan (
    artist_id int REFERENCES artist (id) ON UPDATE CASCADE ON DELETE CASCADE,
    fan_id int REFERENCES fan (id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT artist_fan_pkey PRIMARY KEY (artist_id, fan_id) -- explicit PK
);

create table album_fan (
    album_id int REFERENCES album (id) ON UPDATE CASCADE ON DELETE CASCADE,
    fan_id int REFERENCES fan (id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT album_fan_pkey PRIMARY KEY (album_id, fan_id) -- explicit PK
);

